package config

import java.awt.Color

object ConfigMock {
    var stringProperty1: String = "stringValue1"
    var stringProperty2: String = "stringValue2"
    var longProperty1: Long = 100
    var hashMapProperty1: HashMap<String, Int> = HashMap()
    var nullableColorProperty1: Color? = null
}