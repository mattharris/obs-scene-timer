package objects

object Globals {
    val scenes: ArrayList<TScene> = ArrayList()
    var OBSActivityStatus: OBSStatus? = null
    var OBSConnectionStatus: OBSStatus = OBSStatus.UNKNOWN
}